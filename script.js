/* Відповідь 1:
Екранування дозволяє програмістам включати спеціальні символи, не порушуючи логіку мови програмування.
Без екранування символи можуть викликати недорозуміння або помилки під час компіляції або виконання програми.
 */

/* Відповідь 2:
1)Оголошення за допомогою ключового слова function;
2)Вирази функцій (Function Expressions);
3)Стрілкові функції (Arrow Functions);
4)Стрілкові функції з неявним поверненням (Implicit Return);
5)Функції-конструктори;
6)Методи об'єктів;
7)Функції-генератори (Generator Functions);
 */

/* Відповідь 3:
Hoisting - поведінка JavaScript, при якій оголошення змінних і функцій переносяться вверх до початку свого області видимості перед виконанням коду.
Здається, що оголошення "піднімаються" на початок своїх областей видимості, незалежно від того, де саме знаходиться фактичне оголошення у коді.
При оголошені функції, функція піднімається на початок глобальної області видимості, тому функцію можна викликати навіть перед фактичним оголошенням. 
 */
// Задача:

function createNewUser() {
  let newUser = {};

  newUser.firstName = prompt("Введіть ім'я:");
  newUser.lastName = prompt("Введіть прізвище:");
  newUser.birthday = prompt("Введіть дату народження у форматі mm.dd.yyyy:");

  newUser.getAge = function () {
    const currentDate = new Date();
    const birthDate = new Date(this.birthday);

    let age = currentDate.getFullYear() - birthDate.getFullYear();
    const currentMonth = currentDate.getMonth();
    const birthMonth = birthDate.getMonth();

    if (
      currentMonth < birthMonth ||
      (currentMonth === birthMonth &&
        currentDate.getDate() < birthDate.getDate())
    ) {
      age--;
    }

    return age;
  };

  newUser.getPassword = function () {
    const firstLetterUpper = this.firstName.charAt(0).toUpperCase();
    const lastNameLower = this.lastName.toLowerCase();
    const birthYear = new Date(this.birthday).getFullYear();

    return firstLetterUpper + lastNameLower + birthYear;
  };

  return newUser;
}

let user = createNewUser();
console.log("Створений об'єкт користувача:", user);
console.log("Вік користувача:", user.getAge());
console.log("Пароль користувача:", user.getPassword());
